﻿using Assets.Scripts.Enums;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    public Text GameOverText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var status = GameManager.Instance.BoardScript.Status;

	    if (status == PlayerGameStatus.PlayerWon)
	    {
	        GameOverText.text = "You Won";
	    }
	    else if (status == PlayerGameStatus.PlayerLost)
	    {
	        GameOverText.text = "Game Over";
	    }
	}
}
