﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour {
    public void StartGame()
    {
        SceneManager.LoadScene("menu-team-select");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ReturnToMain()
    {
        SceneManager.LoadScene("menu-main");
    }

}
