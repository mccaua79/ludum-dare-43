﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TeamSelectMenuHandler : MonoBehaviour
{
    public GameObject PlayerErrorText;
    public GameObject EnemyErrorText;

    public void PlayerChooseTeam(int value)
    {
        PlayerErrorText.SetActive(false);
        GameManager.Instance.PlayerTeam = (PlayerTeam) value;
    }

    public void EnemyChooseTeam(int value)
    {
        EnemyErrorText.SetActive(false);
        GameManager.Instance.EnemyTeam = (PlayerTeam)value;
    }

    public void StartGame()
    {
        bool isValid = true;
        PlayerErrorText.SetActive(false);
        EnemyErrorText.SetActive(false);
        if (GameManager.Instance.PlayerTeam == PlayerTeam.None)
        {
            PlayerErrorText.SetActive(true);
            isValid = false;
        }

        if (GameManager.Instance.EnemyTeam == PlayerTeam.None)
        {
            EnemyErrorText.SetActive(true);
            isValid = false;
        }

        if (isValid)
        {
            MenuManager.Instance.StopMusic();
            SceneManager.LoadScene("main"); // Game
        }
    }
}
