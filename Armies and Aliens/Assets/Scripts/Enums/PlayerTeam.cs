﻿public enum PlayerTeam {
    None = 0,
    Tanks = 1,
    Spaceships = 2
}
