﻿namespace Assets.Scripts.Enums
{
    public enum PlayerGameStatus
    {
        None,
        PlayerWon,
        PlayerLost
    }
}
