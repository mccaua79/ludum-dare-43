﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerAttack : MonoBehaviour
{
    public float AttackTimeMinimum;

    public float AttackTimeMaximum;
    private AudioSource _audioSource;
    public AudioClip AudioMissileFire;

    public GameObject AttackPrefab;
    public GameObject PuffSmokePrefab;
    public Transform AttackPoint;

    private bool _isAttacking = false;

    // Use this for initialization
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        var timeUntilNextAttack = Random.Range(AttackTimeMinimum, AttackTimeMaximum);
        // TODO: Use the time to determine when the next instantiate is made
        // TODO: Pass the "strength" along with the AttackPrefab
        if (!_isAttacking)
        {
            Invoke("Attack", timeUntilNextAttack);
            _isAttacking = true;
        }
    }

    public void Attack()
    {
        var targetedEnemy = FindNearestEnemy();
        if (targetedEnemy == null)
        {
            _isAttacking = false;
            return;
        }

        var attackObject = Instantiate(AttackPrefab, AttackPoint.position, Quaternion.identity);
        var projectileBehaviour = attackObject.GetComponent<ProjectileBehaviour>();
        attackObject.tag = gameObject.tag;
        if (projectileBehaviour != null)
        {
            projectileBehaviour.SetTarget(targetedEnemy);
        }

        Instantiate(PuffSmokePrefab, AttackPoint.position, Quaternion.identity);
        _audioSource.PlayOneShot(AudioMissileFire);
        _isAttacking = false;
    }

    public GameObject FindNearestEnemy()
    {
        var tag = gameObject.tag;
        GameObject[] opposingTeam = null;
        if (tag == "Player Team")
        {
            opposingTeam = GameManager.Instance.BoardScript.EnemyTeam;
        }
        else if (tag == "Enemy Team")
        {
            opposingTeam = GameManager.Instance.BoardScript.PlayerTeam;
        }

        if (opposingTeam == null || opposingTeam.Length == 0)
        {
            return null;
        }

        GameObject closest = null;
        for (var i = 0; i < opposingTeam.Length; i++)
        {
            var next = opposingTeam[i];
            if (next == null)
            {
                continue;
            }

            if (closest == null)
            {
                // Cycle through until the "next" enemy is available
                closest = next;
                continue;
            }

            var closestDistance = Vector3.Distance(closest.transform.position, transform.position);
            var nextDistance = Vector3.Distance(next.transform.position, transform.position);
            if (closestDistance > nextDistance)
            {
                closest = next;
            }
        }

        return closest;
    }
}
