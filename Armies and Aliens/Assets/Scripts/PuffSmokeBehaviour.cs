﻿using UnityEngine;

public class PuffSmokeBehaviour : MonoBehaviour {

    public float LiveDelay;
    // Use this for initialization
    void Start()
    {
        Destroy(gameObject, LiveDelay);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
