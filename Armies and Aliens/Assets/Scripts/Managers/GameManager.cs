﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public BoardManager BoardScript;
    public PlayerTeam PlayerTeam;
    public PlayerTeam EnemyTeam;
    
    void Awake()
    {
        BoardScript = GetComponent<BoardManager>();
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += OnSceneLoaded;
        InitGame();
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "main")
        {
            BoardScript.CheckBoard();
        }
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "main")
        {
            BoardScript.SetupScene(PlayerTeam, EnemyTeam);
        }
    }

    public void InitGame()
    {
    }
}
