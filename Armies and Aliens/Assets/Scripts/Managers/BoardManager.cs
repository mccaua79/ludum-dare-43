﻿using Assets.Scripts.Enums;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoardManager : MonoBehaviour
{
    public GameObject Tank;
    public GameObject Alien;
    public GameObject[] PlayerTeam;
    public GameObject[] EnemyTeam;

    public PlayerGameStatus Status;

    public void SetupScene(PlayerTeam playerTeam, PlayerTeam enemyTeam)
    {
        if (playerTeam == global::PlayerTeam.Tanks)
        {
            PlayerTeam = new GameObject[5];
            PlayerTeam[0] = Instantiate(Tank, new Vector3(-6, 3f, 0), Quaternion.identity);
            // TODO: Clean these up (for loop to assign tag)
            PlayerTeam[0].tag = "Player Team";
            PlayerTeam[1] = Instantiate(Tank, new Vector3(-6, 1.5f, 0), Quaternion.identity);
            PlayerTeam[1].tag = "Player Team";
            PlayerTeam[2] = Instantiate(Tank, new Vector3(-6, 0f, 0), Quaternion.identity);
            PlayerTeam[2].tag = "Player Team";
            PlayerTeam[3] = Instantiate(Tank, new Vector3(-6, -1.5f, 0), Quaternion.identity);
            PlayerTeam[3].tag = "Player Team";
            PlayerTeam[4] = Instantiate(Tank, new Vector3(-6, -3f, 0), Quaternion.identity);
            PlayerTeam[4].tag = "Player Team";
        }
        else if (playerTeam == global::PlayerTeam.Spaceships)
        {
            PlayerTeam = new GameObject[5];
            PlayerTeam[0] = Instantiate(Alien, new Vector3(-6, 3f, 0), Quaternion.identity);
            PlayerTeam[0].tag = "Player Team";
            PlayerTeam[1] = Instantiate(Alien, new Vector3(-6, 1.5f, 0), Quaternion.identity);
            PlayerTeam[1].tag = "Player Team";
            PlayerTeam[2] = Instantiate(Alien, new Vector3(-6, 0f, 0), Quaternion.identity);
            PlayerTeam[2].tag = "Player Team";
            PlayerTeam[3] = Instantiate(Alien, new Vector3(-6, -1.5f, 0), Quaternion.identity);
            PlayerTeam[3].tag = "Player Team";
            PlayerTeam[4] = Instantiate(Alien, new Vector3(-6, -3f, 0), Quaternion.identity);
            PlayerTeam[4].tag = "Player Team";
        }


        if (enemyTeam == global::PlayerTeam.Tanks)
        {
            EnemyTeam = new GameObject[5];
            EnemyTeam[0] = Instantiate(Tank, new Vector3(6, 3f, 0), Quaternion.identity);
            EnemyTeam[0].tag = "Enemy Team";
            EnemyTeam[0].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[1] = Instantiate(Tank, new Vector3(6, 1.5f, 0), Quaternion.identity);
            EnemyTeam[1].tag = "Enemy Team";
            EnemyTeam[1].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[2] = Instantiate(Tank, new Vector3(6, 0f, 0), Quaternion.identity);
            EnemyTeam[2].tag = "Enemy Team";
            EnemyTeam[2].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[3] = Instantiate(Tank, new Vector3(6, -1.5f, 0), Quaternion.identity);
            EnemyTeam[3].tag = "Enemy Team";
            EnemyTeam[3].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[4] = Instantiate(Tank, new Vector3(6, -3f, 0), Quaternion.identity);
            EnemyTeam[4].tag = "Enemy Team";
            EnemyTeam[4].GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (enemyTeam == global::PlayerTeam.Spaceships)
        {
            EnemyTeam = new GameObject[5];
            EnemyTeam[0] = Instantiate(Alien, new Vector3(6, 3f, 0), Quaternion.identity);
            EnemyTeam[0].tag = "Enemy Team";
            EnemyTeam[0].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[1] = Instantiate(Alien, new Vector3(6, 1.5f, 0), Quaternion.identity);
            EnemyTeam[1].tag = "Enemy Team";
            EnemyTeam[1].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[2] = Instantiate(Alien, new Vector3(6, 0f, 0), Quaternion.identity);
            EnemyTeam[2].tag = "Enemy Team";
            EnemyTeam[2].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[3] = Instantiate(Alien, new Vector3(6, -1.5f, 0), Quaternion.identity);
            EnemyTeam[3].tag = "Enemy Team";
            EnemyTeam[3].GetComponent<SpriteRenderer>().flipX = true;
            EnemyTeam[4] = Instantiate(Alien, new Vector3(6, -3f, 0), Quaternion.identity);
            EnemyTeam[4].tag = "Enemy Team";
            EnemyTeam[4].GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    public void CheckBoard()
    {
        bool playerIsAlive = false;
        bool enemyIsAlive = false;
        for (int i = 0; i < PlayerTeam.Length; i++)
        {
            if (PlayerTeam[i] != null)
            {
                playerIsAlive = true;
                break;
            }
        }

        for (int i = 0; i < EnemyTeam.Length; i++)
        {
            if (EnemyTeam[i] != null)
            {
                enemyIsAlive = true;
                break;
            }
        }

        if (!playerIsAlive)
        {
            Status = PlayerGameStatus.PlayerLost;
            GameManager.Instance.PlayerTeam = global::PlayerTeam.None;
            GameManager.Instance.EnemyTeam = global::PlayerTeam.None;
            SceneManager.LoadScene("game-over");

            return;
        }

        if (!enemyIsAlive)
        {
            Status = PlayerGameStatus.PlayerWon;
            GameManager.Instance.PlayerTeam = global::PlayerTeam.None;
            GameManager.Instance.EnemyTeam = global::PlayerTeam.None;
            SceneManager.LoadScene("game-over");
        }
    }
}