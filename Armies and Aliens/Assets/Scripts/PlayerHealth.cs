﻿using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float Health;
    public GameObject DeathPrefab;

    private bool _isDead = false;

    public void TakeDamage(float health)
    {
        Health -= health;
        if (Health < 0)
        {
            Health = 0;
            Died();
        }
    }

    public void Died()
    {
        _isDead = true;
    }
    
    public void FixedUpdate()
    {
        if (_isDead)
        {
            // TODO: Give score?
            Instantiate(DeathPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
