﻿using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour
{
    public float Speed;

    public float AttackStrengthMinimum;

    public float AttackStrengthMaximum;

    private GameObject _targetedEnemy;

    private Vector3 _origin;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (_targetedEnemy == null)
	    {
	        Destroy(gameObject);
	    }
	    else
	    {
	        // TODO: Make the projectile have an arc using the origin and the destination
	        transform.position = Vector3.MoveTowards(transform.position, _targetedEnemy.transform.position,
	            Speed * Time.deltaTime);
	    }
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.tag != other.tag)
        {
            HitEnemy(other.gameObject);
            Destroy(gameObject);
        }

        // TODO: Check to see if projectile has hit opposing team's tank
        // TODO: But avoid anything it passes until in reaches the "Destination"
    }

    public void SetTarget(GameObject enemy)
    {
        _targetedEnemy = enemy;
    }

    public void HitEnemy(GameObject target)
    {
        var strengthOfAttack = Random.Range(AttackStrengthMinimum, AttackStrengthMaximum);
        var health = target.GetComponent<PlayerHealth>();
        if (health == null)
        {
            return;
        }

        health.TakeDamage(strengthOfAttack);
    }
}
